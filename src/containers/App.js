import React, { Component } from "react";
import "../styles/App.css";
import PlayerContainer from "./PlayerContainer";

class App extends Component {
  render() {
    return (
      <PlayerContainer
        url={"https://media.w3.org/2010/05/sintel/trailer_hd.mp4"}
        hotspots={hotspots}
        poster={require("../assets/images/imageone.png")}
      />
    );
  }
}

export default App;

const hotspots = [
  {
    thumbnail: require("../assets/images/imageone.png"),
    description:
      "In the beginning she was wondering,in the wilderness searching...",
    time: "00:00:05",
    length: 3
  },
  {
    thumbnail: require("../assets/images/imagetwo.png"),
    description:
      "Then she master the master,the God father of the gate keepers..",
    time: "00:00:12",
    length: 1
  }
];
