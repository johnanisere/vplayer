import React, { Component } from "react";
import cx from "classnames";
import Player from "./player";
import PropTypes from "prop-types";
import Hotspots from "./hotspots";
import moment from "moment";
class Progress extends Component {
  static propTypes = {
    buffered: PropTypes.number,
    progress: PropTypes.number,
    player: PropTypes.instanceOf(Player),
    className: PropTypes.string
  };

  static defaultProps = {
    buffered: 0,
    progress: 0
  };

  shouldComponentUpdate(nextProps) {
    let { buffered, progress } = this.props;
    return buffered !== nextProps.buffered || progress !== nextProps.progress;
  }

  handleSeek = e => {
    e.preventDefault();
    let { player } = this.props;
    if (player && !isNaN(player.video.duration)) {
      player.seek(e);
    }
  };

  myStyle = (time, duration, length) => {
    const myTime = moment.duration(time, "seconds").seconds();

    const left = `${myTime / duration * 100}%`;
    const width = `${length / duration * 100}%`;
    const maxProgress = length / duration * 100 + myTime / duration * 100;

    const myLeft = myTime / duration * 100;

    return { left, width, maxProgress, myLeft };
  };

  render() {
    let { buffered, progress, className, player, hotspots } = this.props;
    let duration =
      player && !isNaN(player.video.duration) ? player.video.duration : "";

    if (progress < 0) {
      progress = 0;
    }
    if (progress > 100) {
      progress = 100;
    }
    let barStyle = { width: `${progress}%` };
    let bufferStyle = { width: `${buffered}%` };
    const classNames = cx("rp-progress", className);

    return (
      <div className={classNames} onClick={this.handleSeek}>
        <div className="rp-progress-bar" style={barStyle}>
          <div className="rp-progress-scrub" />
        </div>
        <div className="rp-progress-buffer" style={bufferStyle} />
        <Hotspots
          myStyle={this.myStyle}
          progress={progress}
          duration={duration}
          hotspots={hotspots}
        />
      </div>
    );
  }
}

export default Progress;
