import React from "react";
import Hotspotmodal from "./hotspotmodal";

const myHotspots = ({ myStyle, progress, duration, hotspots }) => (
  <div className="rp-hotspots-holder">
    {hotspots.map((val, key) => (
      <Hotspotmodal
        progress={progress}
        left={duration ? myStyle(val.time, duration, val.length).myLeft : ""}
        maxProgress={
          duration ? myStyle(val.time, duration, val.length).maxProgress : ""
        }
        duration={duration}
        myStyle={myStyle}
        key={key}
        val={val}
      />
    ))}
  </div>
);
export default myHotspots;
