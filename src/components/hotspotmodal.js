import React from "react";

const hotspotmodal = ({
  progress,
  left,
  maxProgress,
  duration,
  myStyle,
  val
}) =>
  duration ? (
    <div
      className="rp-progress-hotspot"
      style={
        duration
          ? {
              width: myStyle(val.time, duration, val.length).width,
              left: myStyle(val.time, duration, val.length).left
            }
          : {}
      }
    >
      {progress >= left && progress <= maxProgress ? (
        <div className="hotspot--modal">
          <span
            className="hotspot--image"
            style={{
              backgroundImage: `url(${val.thumbnail})`
            }}
          />
          <span className="hotspot--description">{val.description}</span>
          <span className="hotspot--pointer" />
        </div>
      ) : null}
    </div>
  ) : null;

export default hotspotmodal;
